<?php

/**
 * @file
 * Primary module hooks for block_content_suggestions module.
 */

use Drupal\Core\Render\Element;
use Drupal\block_content_suggestions\BlockContentViewBuilder;

/**
 * Implements hook_theme().
 */
function block_content_suggestions_theme() {
  return [
    'block_content' => [
      'render element' => 'elements',
    ],
  ];
}

/**
 * Implements hook_entity_type_alter().
 */
function block_content_suggestions_entity_type_alter(array &$entity_types) {
  /** @var \Drupal\Core\Entity\EntityTypeInterface[] $entity_types */
  $entity_types['block_content']->setHandlerClass('view_builder', BlockContentViewBuilder::class);
}

/**
 * Implements hook_theme_suggestions_HOOK().
 */
function block_content_suggestions_theme_suggestions_block_content(array $variables) {
  $suggestions = [];
  $block_content = $variables['elements']['#block_content'];
  $sanitized_view_mode = strtr($variables['elements']['#view_mode'], '.', '_');

  $suggestions[] = 'block_content__' . $sanitized_view_mode;
  $suggestions[] = 'block_content__' . $block_content->bundle();
  $suggestions[] = 'block_content__' . $block_content->bundle() . '__' . $sanitized_view_mode;
  $suggestions[] = 'block_content__' . $block_content->id();
  $suggestions[] = 'block_content__' . $block_content->id() . '__' . $sanitized_view_mode;

  return $suggestions;
}

/**
 * Prepares variables for block_content templates.
 *
 * Default template: block-content.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An array of elements to display in view mode.
 *   - block_content: The block_content object.
 *   - view_mode: View mode; e.g., 'full', 'teaser'...
 */
function template_preprocess_block_content(array &$variables) {
  $variables['view_mode'] = $variables['elements']['#view_mode'];
  $variables['block_content'] = $variables['elements']['#block_content'];

  // Helpful $content variable for templates.
  $variables += ['content' => []];
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
